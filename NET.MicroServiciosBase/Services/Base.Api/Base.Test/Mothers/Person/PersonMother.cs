﻿namespace Test.Mothers.Person
{
    using Base.Api.Data.Entities;

    public static class PersonMother
    {
        public static Base.Api.Data.Entities.Person Default => new Person(1, "Alejandro", "M");
        public static Base.Api.Data.Entities.Person DefaultWithError => new Person(2, "Ale%", "M");
    }
}