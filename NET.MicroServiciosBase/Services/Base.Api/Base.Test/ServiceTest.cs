﻿using Base.Api.Data;
using Base.Api.Data.Repositories.Person;
using Castle.Core.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Test
{
    public class ServiceTest
    {
        // Instance the dependences needly for handler tests
        protected ApplicationDbContext _dbContext;
        public PersonRepository _personRepository;

        public ServiceTest()
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseSqlServer("Data Source=DESKTOP-6PNNR53\\LOCALHOST;User ID=sa;Password=sa123;Connection Timeout=3600;Initial Catalog=TestDb;MultipleActiveResultSets=True;TrustServerCertificate=True");
            var dbContextOptions = builder.Options;
            _dbContext = new ApplicationDbContext(dbContextOptions);
            _personRepository = new PersonRepository(_dbContext);
            //_dbContext.Database.EnsureDeleted();
            //_dbContext.Database.EnsureCreated();
        }

        //public IGenericReadRepository GetInMemoryReadRepository()
        //{
        //    return new GenericReadRepository(libraryDbContext);
        //}

        //public IGenericWriteRepository GetInMemoryWriteRepository()
        //{
        //    return new GenericWriteRepository(libraryDbContext);
        //}

        //public IEnumerable<Author> GetMockAuthors()
        //{
        //    return new List<Author>()
        //    {
        //        { new Author(){ Id = 1, FirstName = "William", LastName = "Shakespear"} },
        //        { new Author(){ Id = 2, FirstName = "Charles", LastName = "Dickens"} },
        //        { new Author(){ Id = 3, FirstName = "George", LastName = "Eliot"} }
        //    };
        //}
    }
}

