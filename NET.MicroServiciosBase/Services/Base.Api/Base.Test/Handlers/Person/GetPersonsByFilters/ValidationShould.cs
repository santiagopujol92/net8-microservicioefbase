﻿using App.Core.Person.GetPersonsByFilters;
using FluentValidation;
using FluentValidation.TestHelper;
using Moq;
using Xunit;

namespace Test.Handlers.Person.GetPersonsByFilters;

public class ValidationShouldPerson
{
    protected GetPersonsByFiltersRequestValidator _validator;

    public ValidationShouldPerson()
    {
        _validator = new GetPersonsByFiltersRequestValidator();
        ValidatorOptions.Global.DefaultClassLevelCascadeMode = CascadeMode.Stop;
        ValidatorOptions.Global.DefaultRuleLevelCascadeMode = CascadeMode.Stop;
    }

    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData(null)]
    public void Have_Error_When_Name_IsEmpty(string name)
    {
        var request = new GetPersonsByFiltersRequest()
        {
            Name = name,
            Gender = "M"
        };

        var result = _validator.TestValidate(request);
        result.ShouldHaveValidationErrorFor(x => x.Name);
    }

    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData(null)]
    public void Have_Error_When_Gender_IsEmpty(string gender)
    {
        var request = new GetPersonsByFiltersRequest()
        {
            Name = "Alejandro",
            Gender = gender
        };

        var result = _validator.TestValidate(request);
        result.ShouldHaveValidationErrorFor(x => x.Gender);
    }
}