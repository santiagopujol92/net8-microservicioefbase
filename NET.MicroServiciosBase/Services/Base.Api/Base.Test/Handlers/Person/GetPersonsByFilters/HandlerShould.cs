﻿namespace Test.Handlers.Person.GetPersonsByFilters
{
    using App.Core.Person.GetPersonsByFilters;
    using AutoMapper;
    using Base.Api.Data.Repositories.Person;
    using Moq;
    using Test.Mothers.Person;
    using Test.Repositories;
    using Xunit;
    using Base.Api.Data.Entities;
    using Base.Api.Data;

    public class HandlerShouldPerson : ServiceTest
    {
        protected Mock<IMapper> MapperMock;
        protected Mock<IPersonRepository> PersonRepositoryServiceMock;
        protected GetPersonsByFiltersHandler Handler;

        public HandlerShouldPerson()
        {
            MapperMock = new Mock<IMapper>();
            PersonRepositoryServiceMock = PersonRepositoryServiceMockFactory.Default;
            Handler = new GetPersonsByFiltersHandler(PersonRepositoryServiceMock.Object, MapperMock.Object);
        }

        [Fact]
        public void ReturnThrowException_GetPersonsByFilters()
        {
            var persons = new List<Person> { PersonMother.DefaultWithError };

            PersonRepositoryServiceMock.Setup(service => service
                .GetByFilters("Ale%", "M"))
                .ReturnsAsync(persons);

            var request = new GetPersonsByFiltersRequest()
            {
                Name = "Ale%",
                Gender = "M"
            };

            Assert.ThrowsAnyAsync<HttpRequestException>(() => Handler.Handle(request, CancellationToken.None));
        }

        [Fact]
        public async void ReturnData_GetPersonsByFilters()
        {
            var request = new GetPersonsByFiltersRequest()
            {
                Name = "Santiago",
                Gender = "M"
            };

            var response = await _personRepository.GetByFilters(request.Name, request.Gender);
            var responseData = response;
            Assert.NotEmpty(responseData);
        }
    }

}
