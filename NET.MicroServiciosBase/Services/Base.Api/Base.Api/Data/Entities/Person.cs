﻿namespace Base.Api.Data.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.Json.Serialization;

    [Table("persons", Schema = "dbo")]
    public class Person
    {
        [Key]
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("gender")]
        public string? Gender { get; set; }

        [JsonPropertyName("name")]
        public string? Name { get; set; }

        public Person(int id, string gender, string name)
        {
            Id = id;
            Gender = gender;
            Name = name;
        }
    }

}