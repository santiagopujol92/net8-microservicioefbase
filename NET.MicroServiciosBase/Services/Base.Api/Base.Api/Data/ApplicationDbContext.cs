﻿namespace Base.Api.Data
{
    using Microsoft.EntityFrameworkCore;
    using Base.Api.Data.Entities;
    using Base.Api.Data.EntityConfiguration;

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            PersonEntityConfiguration
                .Configure(modelBuilder.Entity<Person>());

            base.OnModelCreating(modelBuilder);
        }
    }
}
