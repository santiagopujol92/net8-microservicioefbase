﻿namespace Base.Api.Data.Repositories.Person
{
    public interface IPersonRepository
    {
        Task<IEnumerable<Entities.Person>> GetByFilters(string name, string gender);
    }
}
