﻿namespace Base.Api.Data.Repositories.Person
{
    using Microsoft.EntityFrameworkCore;
    using Entities;

    public class PersonRepository : IPersonRepository
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly DbSet<Person> _personEntity;

        public PersonRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _personEntity = _dbContext.Set<Person>();
        }

        public async Task<IEnumerable<Person>> GetByFilters(string name, string gender)
        {
            return await _personEntity.Where(e => e.Name == name && e.Gender == gender).ToListAsync();
        }
    }
}
