﻿namespace Base.Api.Data.EntityConfiguration
{
    using Base.Api.Data.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PersonEntityConfiguration
    {
        public static void Configure(EntityTypeBuilder<Person> entityBuilder)
        {
            entityBuilder.HasKey(e => e.Id);
        }
    }
}
