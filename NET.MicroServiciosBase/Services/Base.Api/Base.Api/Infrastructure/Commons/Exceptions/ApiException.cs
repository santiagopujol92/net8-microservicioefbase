﻿namespace Base.Api.Infrastructure.Commons.Exceptions
{
    using Base.Api.Infrastructure.Commons.Api;
    /// <summary>
    /// Represent an exception occurr in application
    /// </summary>
    public class ApiException : Exception
    {
        public int StatusCode { get; set; }

        public string Status => StatusCode switch
        {
            400 => "bad_request",
            404 => "not_found",
            500 => "bad_request",
            _ => "internal_server_error",
        };

        public string? Code { get; set; }

        public string? Help { get; set; }

        private List<Message>? Messages { get; set; }

        public BaseApiResponse<object> BaseApiResponse
        {
            get
            {
                List<Message> list = Messages;
                if (list == null)
                {
                    var obj = new List<Message>
                {
                    new() {
                        Status = !string.IsNullOrEmpty(Status) ? Status : null,
                        StatusCode = !string.IsNullOrEmpty(Code) ? int.Parse(Code) : null,
                        Text = Message,
                        Help = !string.IsNullOrEmpty(Help) ? Help : null
                    }
                };
                    List<Message> list2 = obj;
                    Messages = obj;
                    list = list2;
                }

                return new BaseApiResponse<object>(list);
            }
        }

        public ApiException(string message)
            : base(message)
        {
            StatusCode = 500;
        }

        public ApiException(int statusCode, string message)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public ApiException(int statusCode, List<Message> messages)
        {
            StatusCode = statusCode;
            Messages = messages;
        }

        public ApiException(int statusCode, string message, Exception exception)
            : base(message, exception)
        {
            StatusCode = statusCode;
        }

        public ApiException(int statusCode, string message, Exception exception, string code)
            : base(message, exception)
        {
            StatusCode = statusCode;
            Code = code;
        }

        public ApiException(int statusCode, string message, Exception exception, string code, string help)
            : base(message, exception)
        {
            StatusCode = statusCode;
            Code = code;
            Help = help;
        }

        public ApiException(int statusCode, string message, string code, string help)
            : base(message)
        {
            StatusCode = statusCode;
            Code = code;
            Help = help;
        }
    }
}