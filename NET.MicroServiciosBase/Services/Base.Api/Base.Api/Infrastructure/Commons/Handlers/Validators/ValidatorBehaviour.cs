﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Serilog;

namespace Base.Api.Infrastructure.Commons.Handlers.Validators
{
    public class ValidatorBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public ValidatorBehaviour(IEnumerable<IValidator<TRequest>> validators)
        {
            _validators = validators;
        }

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            if (_validators.Any())
            {
                ValidationContext<TRequest> context = new ValidationContext<TRequest>(request);
                List<ValidationFailure> list = (from f in (await Task.WhenAll(_validators.Select((v) => v.ValidateAsync(context, cancellationToken)))).SelectMany((r) => r.Errors)
                                                where f != null
                                                select f).ToList();
                if (list.Any())
                {
                    Log.Error("validation in type {@Type} failed: {@Failures}", typeof(TRequest).ShortDisplayName(), list.Select((f) => f.ErrorMessage));
                    throw new ValidationException(list);
                }
            }

            return await next();
        }
    }
}