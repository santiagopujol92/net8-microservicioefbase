﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Base.Api.Infrastructure.Commons.Api;

public class BaseApiResponseActionFilterAttribute : ActionFilterAttribute
{
    public override void OnResultExecuting(ResultExecutingContext context)
    {
        if (context.Result is OkObjectResult okObjectResult)
        {
            object value = okObjectResult.Value;
            Type type = value?.GetType() ?? typeof(object);
            if (!type.Name.Contains("BaseApiResponse"))
            {
                _ = okObjectResult.Value = Activator.CreateInstance(typeof(BaseApiResponse<>).MakeGenericType(type), value, new Message(HttpStatusCode.OK, "Execution Successfully."));
            }
        }

        if (context.Result is BadRequestObjectResult badRequestObjectResult)
        {
            badRequestObjectResult.Value = new BaseApiResponse<object>(new Message(HttpStatusCode.BadRequest, badRequestObjectResult.Value?.ToString()));
        }

        if (context.Result is NotFoundObjectResult notFoundObjectResult)
        {
            notFoundObjectResult.Value = new BaseApiResponse<object>(new Message(HttpStatusCode.NotFound, notFoundObjectResult.Value?.ToString()));
        }

        base.OnResultExecuting(context);
    }
}