﻿using Base.Api.Infrastructure.Commons.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Base.Api.Infrastructure.Commons.Api;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json", [])]
[TypeFilter(typeof(BaseApiResponseActionFilterAttribute))]
[ProducesResponseType(400, Type = typeof(BaseApiResponse<>))]
[ProducesResponseType(204, Type = typeof(BaseApiResponse<>))]
[ProducesResponseType(404, Type = typeof(BaseApiResponse<>))]
[ProducesResponseType(500, Type = typeof(BaseApiResponse<>))]
public class ApiControllerBase : Controller
{
    protected async Task<T> HandleRequestAsync<T>(IRequest<T> request)
    {
        IMediator service = HttpContext.RequestServices.GetService<IMediator>();
        CancellationToken requestAborted = HttpContext.RequestAborted;

        if (service == null)
            throw new BadRequestApiException("Mediator service not exist.");

        return await service.Send(request, requestAborted);
    }
}