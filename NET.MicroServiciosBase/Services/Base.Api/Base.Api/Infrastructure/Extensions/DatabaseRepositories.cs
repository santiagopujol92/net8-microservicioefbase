﻿using Base.Api.Data;
using Base.Api.Data.Repositories.Person;
using Microsoft.EntityFrameworkCore;

namespace App.Infrastructure.Extensions;

public static class DatabaseRepositoriesExtensions
{
    public static IServiceCollection AddDatabaseRepositories(this IServiceCollection services, IConfiguration configuration)
    {
        //Add context Database references
        services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("SqlServerConnection")));

        //Add Repositories of models
        services.AddScoped<IPersonRepository, PersonRepository>();

        return services;
    }
}