﻿using Microsoft.OpenApi.Models;

namespace App.Infrastructure.Extensions;

public static class SwaggerExtensions
{
    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(opt =>
        {
            opt.OrderActionsBy(p => p.RelativePath);
            opt.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = $"App API",
                Description = $"An ASP.NET Core Web API for managing App items",
            });
        });
        return services;
    }
}
