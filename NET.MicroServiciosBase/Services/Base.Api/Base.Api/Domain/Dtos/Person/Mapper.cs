﻿
namespace Base.Api.Domain.Dtos.Person
{
    using AutoMapper;
    public class PersonProfile : Profile
    {
        public PersonProfile()
        {
            CreateMap<Data.Entities.Person, PersonDto>();
        }
    }
}