﻿namespace Base.Api.Domain.Dtos.Person
{
    public class PersonDto
    {
        public int? Id { get; set; }
        public string? Gender { get; set; }
        public string? Name { get; set; }
    }
}